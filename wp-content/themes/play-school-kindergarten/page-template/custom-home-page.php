<?php
/**
 * Template Name: Custom Home
 */

get_header(); ?>

<?php do_action( 'play_school_kindergarten_above_product_page' ); ?>

<section>
	<?php
	    // Get pages set in the customizer (if any)
	    $pages = array();
	    for ( $count = 1; $count <= 4; $count++ ) {
	    $mod = absint( get_theme_mod( 'play_school_kindergarten_slide_page' . $count ));
	    if ( 'page-none-selected' != $mod ) {
	      $pages[] = $mod;
	    }
	    }
	    if( !empty($pages) ) :
	      $args = array(
	        'posts_per_page' => 5,
	        'post_type' => 'page',
	        'post__in' => $pages,
	        'orderby' => 'post__in'
	      );
	      $query = new WP_Query( $args );
	      if ( $query->have_posts() ) :
	        $count = 1;
	        ?>
	    <div class="slider-main">
	          <div id="slider" class="nivoSlider">
	            <?php
	              $play_school_kindergarten_n = 0;
	          while ( $query->have_posts() ) : $query->the_post();
	            
	            $play_school_kindergarten_n++;
	            $play_school_kindergarten_slideno[] = $play_school_kindergarten_n;
	            $play_school_kindergarten_slidetitle[] = get_the_title();
	            $play_school_kindergarten_content[] = get_the_excerpt();
				$play_school_kindergarten_slidelink[] = esc_url(get_permalink());
	            ?>
	              <img src="<?php the_post_thumbnail_url('full'); ?>" title="#slidecaption<?php echo esc_attr( $play_school_kindergarten_n ); ?>" />
	            <?php
	          $count++;
	          endwhile; ?>
	          </div>

	        <?php
	        $play_school_kindergarten_k = 0;
	          foreach( $play_school_kindergarten_slideno as $play_school_kindergarten_sln ){ ?>
	          	<div id="slidecaption<?php echo esc_attr( $play_school_kindergarten_sln ); ?>" class="nivo-html-caption">
		            <div class="slide-cap  ">
		              	<div class="container">
			                <h2><?php echo esc_html($play_school_kindergarten_slidetitle[$play_school_kindergarten_k] ); ?></h2>
			                <p><?php echo esc_html($play_school_kindergarten_content[$play_school_kindergarten_k] ); ?></p>
			                <div class="read-more">
			                	<a href="<?php echo esc_url($play_school_kindergarten_slidelink[$play_school_kindergarten_k] ); ?>"><?php esc_html_e( 'Read More','play-school-kindergarten' ); ?></a>
			                </div>
		              	</div>
		            </div>
	          	</div>
	            <?php $play_school_kindergarten_k++;
	        } ?>
	    </div>
        <?php else : ?>
          <div class="header-no-slider"></div>
        <?php
      endif;
    endif;
  	?>
</section>

<?php do_action('play_school_kindergarten_befor_about_section'); ?>

<?php /*--About Us--*/?>
<section class="about">
  	<div class="container">
	    <h3><a href="<?php the_permalink();?>"><?php echo esc_html(get_theme_mod('play_school_kindergarten_about_name',__('Welcome','play-school-kindergarten'))); ?></a></h3>
	    <?php
	      	$args = array( 'name' => get_theme_mod('play_school_kindergarten_about_setting',''));
	      	$query = new WP_Query( $args );
	      	if ( $query->have_posts() ) :
		        while ( $query->have_posts() ) : $query->the_post(); ?>
			    	<div class="row">
			          	<div class="col-lg-8 col-md-8">
				            <h4><?php the_title(); ?></h4>
				            <p><?php the_excerpt(); ?></p>
			          	</div>
			          	<div class="col-lg-4 col-md-4">
				            <div class="abt-image">
				              <img src="<?php the_post_thumbnail_url('full'); ?>"/>
				            </div>
				        </div>
			        </div>
		        <?php endwhile; 
	        	wp_reset_postdata();?>
	        <?php else : ?>
	          <div class="no-postfound"></div>
	        <?php endif; 
		?>
  	</div>
</section>

<?php do_action('play_school_kindergarten_after_about_section'); ?>

<div class="container">
  <?php while ( have_posts() ) : the_post(); ?>
        <?php the_content(); ?>
    <?php endwhile; // end of the loop. ?>
</div>

<?php get_footer(); ?>