=== Play School Kindergarten ===
Contributors: Luzuk
Tags: one-column, two-columns, right-sidebar, left-sidebar, grid-layout, custom-colors, custom-background, custom-header, custom-menu, custom-logo, editor-style, featured-images, footer-widgets, rtl-language-support, post-formats, full-width-template, threaded-comments, translation-ready, blog, education, e-commerce
Requires at least: 4.8
Tested up to: 5.2.2
Requires PHP: 7.2.14
Stable tag: 0.3.2
License: GNU General Public License v3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Play School Kindergarten is a beautiful and professional WordPress theme made for kindergartens, nurseries, play schools, preschools, childcare centres, elementary and primary schools. It can be used by people providing online courses and e-learning. You can also use this multipurpose theme if you run a training centre or any educational institutions. The theme serves the dedicated purpose of designing a site pertaining to children with its fun images and bright colours. Its colour scheme is so chosen to give it a fresh look as required for a child theme.

== Description ==

Play School Kindergarten is a beautiful and professional WordPress theme made for kindergartens, nurseries, play schools, preschools, childcare centres, elementary and primary schools. It can be used by people providing online courses and e-learning. You can also use this multipurpose theme if you run a training centre or any educational institutions. The theme serves the dedicated purpose of designing a site pertaining to children with its fun images and bright colours. Its colour scheme is so chosen to give it a fresh look as required for a child theme. The theme is fully responsive and cross-browser compatible to serve a wide range of audience. It is translation ready to satisfy various demographics with ease. Though it is fun-filled but that does not make it messy. It has a clean design and loads faster. Short codes are implemented to make its coding further easy and clean. The Play School Kindergarten theme is customizable which allows you to make mdall changes to it on your own. You can change its background, colour, logo, tagline etc. according to your requirements. It has dedicated header and footer area to add elements to it. It is an SEO-friendly theme which has social media integration to share your content on various networking platforms.

== Changelog ==

= 0.1 =
* Initial Version Released.

= 0.2 =
* Resolved minor theme errors.
* Done minor css customization.

= 0.3 =
* Resolved minor error occured in files.
* Done css customization in footer.

= 0.3.1 =
* Added customizer license in readme.txt file
* Fixed the second and third level menu issue.

= 0.3.2 =
* Added POT file.
* Changed Screenshot.
* Added Post Formats.
* Changed in readme file.

== Resources ==

*	Play School Kindergarten WordPress Theme, Copyright 2018 Luzuk
	Play School Kindergarten is distributed under the terms of the GNU GPL

*	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 2 of the License, or
	(at your option) any later version.

*	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
	GNU General Public License for more details.

*	Play School Kindergarten WordPress Theme is derived from Twenty Seventeen WordPress Theme, Copyright 2016 WordPress.org
	Twenty Seventeen WordPress Theme is distributed under the terms of the GNU GPL

*	Play School Kindergarten bundles the following third-party resources:

*	All js that have been used are within folder /js of theme.
	jquery.nivo.slider.js, License: MIT, License Url: https://opensource.org/licenses/MIT

*	CSS bootstrap.css
	Code and documentation copyright 2011-2016 Twitter, Inc. Code released under the MIT license. Docs released under Creative Commons.

*	CSS nivo-slider.css
	Copyright 2012-2017, Dev7studios
	Free to use and abuse under the MIT license.
	http://www.opensource.org/licenses/mit-license.php

*	HTML5 Shiv, Copyright 2014 Alexander Farkas
	Licenses: MIT/GPL2
	Source: https://github.com/aFarkas/html5shiv

*	jQuery scrollTo, Copyright 2007-2015 Ariel Flesler
	License: MIT
	Source: https://github.com/flesler/jquery.scrollTo

*	normalize.css, Copyright 2012-2016 Nicolas Gallagher and Jonathan Neal
	License: MIT
	Source: https://necolas.github.io/normalize.css/

*	Font Awesome icons, Copyright Dave Gandy
	License: SIL Open Font License, version 1.1.
	Source: http://fontawesome.io/

*	Screenshot Images.
	License: CC0 1.0 Universal (CC0 1.0) 
	Source: https://stocksnap.io/license

	Slider image, Copyright Freestocks.org
	License: CC0 1.0 Universal (CC0 1.0)
	Source: https://stocksnap.io/photo/A8DQ8W3OCI

	Slider image, Copyright Ben White
	License: CC0 1.0 Universal (CC0 1.0)
	Source: https://stocksnap.io/photo/XDNPCSL1U2

	About image, Copyright Olu Eletu
	License: CC0 1.0 Universal (CC0 1.0)
	Source: https://stocksnap.io/photo/Q5FJUK9OFH

*	Customizer Pro, Copyright 2016 © Justin Tadlock.
	License: All code, unless otherwise noted, is licensed under the GNU GPL, version 2 or later.
	Source: https://github.com/justintadlock/trt-customizer-pro
